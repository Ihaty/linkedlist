import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LListTest {

    private LList lList;
    private final int[] ARR_CHECK = {4,5,8,100,7};

    LListTest(){
        lList = new LList();
    }

    @BeforeEach
    void TestL(){
        lList.add(5);
        lList.add(6);
        lList.add(7);
        lList.add(50);
        lList.add(100);
        lList.add(68);
    }

    @Test
    void testSize(){
        int exp = 6;
        int act = lList.size();
        assertEquals(exp, act);
    }

    @Test
    void testClear(){
        lList.clear();
        int exp = 0;
        int act = lList.size();
        assertEquals(exp, act);
    }

    @Test
    void testToArray(){
        int[] exp = {5,6,7,50,100,68};
        int[] act = lList.toArray();
        assertArrayEquals(exp,act);
    }

    @Test
    void testaddIndex(){
        lList.add(3,1000);
        int exp1 = 1000;
        int act1 = lList.get(3);
        int[] exp2 = {5,6,7,1000,50,100,68};
        int[] act2 = lList.toArray();
        assertEquals(exp1, act1);
        assertArrayEquals(exp2,act2);
    }


    @Test
    void testSubList(){
        int[] exp = {7,50,100};
        int[] act = lList.subList(2,5);
        assertArrayEquals(exp,act);
    }

    @Test
    void testRemoveAll(){
        lList.removeAll(ARR_CHECK);
        int[] exp = {6,50,68};
        int[] act = lList.toArray();
        assertArrayEquals(exp,act);
    }

    @Test
    void testRetainAll(){
        lList.retainAll(ARR_CHECK);
        int[] exp = {5,7,100};
        int[] act = lList.toArray();
        assertArrayEquals(exp,act);
    }
}
